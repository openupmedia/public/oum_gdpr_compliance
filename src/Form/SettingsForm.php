<?php

/**
 * @file
 * Contains Drupal\oum_gdpr_compliance\Form\SettingsForm.
 */

namespace Drupal\oum_gdpr_compliance\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\oum_gdpr_compliance\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'oum_gdpr_compliance.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oum_gdpr_compliance_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('oum_gdpr_compliance.settings');

    $form['oum_gdpr_compliance_styling_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Styling settings'),
      '#open' => TRUE,
    ];

    $form['oum_gdpr_compliance_styling_settings']['load_module_twig'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable default twig template provided by this module'),
      '#default_value' => $config->get('load_module_twig'),
      '#description' => t('Loads a twig template file for the default OUM layout & styling (included in the module).<br /><strong>Note:</strong> changing this value requires a cache clear in order to work!'),
    ];

    $form['oum_gdpr_compliance_styling_settings']['load_module_css'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable default CSS provided by this module'),
      '#default_value' => $config->get('load_module_css'),
      '#description' => t('Loads a CSS file for the default OUM layout & styling (included in the module).'),
    ];

    $form['oum_gdpr_compliance_content_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Content settings'),
      '#open' => TRUE,
    ];

    $form['oum_gdpr_compliance_content_settings']['accept_minimal'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable "Only accept necessary cookies" button (Deprecated)'),
      '#default_value' => $config->get('accept_minimal'),
      '#description' => t('Enables an extra button for the twig template.<br />You can use <code>accept_minimal</code> in your twig files.'),
      '#access' => FALSE,
    ];

    $form['oum_gdpr_compliance_content_settings']['manage_categories_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Change preferences" button label'),
      '#default_value' => $config->get('manage_categories_label'),
      '#description' => $this->t('The label of the "Change preferences" button.'),
    ];


    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    // By default, render the form using system-config-form.html.twig.
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Save the values.
    $config = $this->config('oum_gdpr_compliance.settings');
    $config->set('load_module_twig', $form_state->getValue('load_module_twig'))
      ->set('load_module_css', $form_state->getValue('load_module_css'))
      ->set('accept_minimal', $form_state->getValue('accept_minimal'))
      ->set('manage_categories_label', $form_state->getValue('manage_categories_label'))
      ->save();

    parent::submitForm($form, $form_state);

    Cache::invalidateTags(['oum_gdpr_compliance:attachments']);
  }

}
