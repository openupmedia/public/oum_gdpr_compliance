# Open Up Media GDPR compliance
This module is the successor of oum_cookie_compliance. It adds privacy enhancements required for openup.media projects, by extending the eu_cookie_compliance, google_tag and other modules with custom dependencies, settings & logic.

## Features
- Implements best practice settings for eu_cookie_compliance.
- Creates 3 cookie categories for essential, analytical and marketing/ad cookies.
- Adds Google Consent mode v2 through eu_cookie_compliance_gtm
- ~~Adds a default "global" GTM container.~~ -> Disabled for now, not working correctly.
- Allows reopening of the info popup via any link with the "#eucc-open" fragment.
- Implements a Chance Preferences button label, which can be used in the popup twig.

## Requirements
The eu_cookie_compliance, eu_cookie_compliance_gtm & google_tag modules are required.

## Installation
This module can be installed using Composer through Open Up Media's private package repository: https://packages.openupmedia.be.

To do this, add the package repository to the project's `composer.json`
file by adding the following

```php
{
  "repositories": [{
    "type": "composer",
    "url": "https://packages.openupmedia.be/"
  }]
}
```

or run
```sh
composer config repositories.openupmedia composer https://packages.openupmedia.be/
```

You'll then be able to install the module with
```sh
composer require "openupmedia/oum_gdpr_compliance":"^1.0@beta"
```

Reload optional config (correct cookie categories, etc)
```sh
drush cim --source=modules/contrib/oum_gdpr_compliance/config/optional --partial
```

## What does this module do

### EU Cookie Compliance
This module loads our required settings config for the eucc module, and installs 3 categories,
which we require on all sites (only if theses categories don't exist), and introduces a reopen popup possibility.

**Included categories**
1. Essential cookies (essential)
2. Analytical cookies (analytics)
3. Marketing / Ad cookies (marketing)

These categories are also used in the Google Consent Mode mapping.

We also support 3 additional categories, which can be added manually:
`functional`, `personalization` & `security`.

An overview of the cookies and translations can be found here: https://www.notion.so/openupmedia/Privacy-cookies-908850d28943437999261af8d0e67c2d

**Reopen the cookie popup**
Allows reopening of the info popup via any link with the `#eucc-open` fragment

### Google Consent Mode
We set a default Google Consent Mode (`analytics_storage` & `marketing_storage` are both set so `denied`). With this setting we can safely activate Google Analytics or other Google services, and they will comply with GDPR. No cookies will be set, and no identifying data is stored. When the user accepts the analytics or marketing categories, we update the corresponding Google Consent Mode storage value to `granted` so the Google Services can start tracking personal data, store cookies, etc.

We also send a `cookie_consent` event to the datalayer, containing the cookie categories, and whether they are allowed or not.
This event can be used to create custom triggers, to trigger additional tags, like social media pixels, etc.

The supported triggers are: `analytics_consent`, `marketing_consent`, `functional_consent`, `personalization_consent` & `security_consent`.


## TODO
- Translations:
  - [x] Custom translation strings (.po files)
  - [ ] Config translations: not working for now
- Add custom form + tokens for privacy & cookie nodes / or use system_tags
- Add custom CSS
- Clean up template file
- Optimize accessibility: https://git.drupalcode.org/project/eu_cookie_compliance_rocketship/-/blob/1.0.x/js/eu-cookie-compliance-rocketship.js#L114

## Future & thoughts

1. Do we want to check for existing cookie categories on install, and remove them?
2. Should we delete cookie categories when uninstalling this module (code is commented out in .install)?
_Current opinion:_ No, as we might uninstall this module in the future, in favor of another contrib solution, that might need these categories.
3. Should we require [eu_cookie_compliance_rocketship](https://www.drupal.org/project/eu_cookie_compliance_rocketship), as it has most features. But it also implements some unwanted features.
4. Implement [cookie_content_blocker](https://www.drupal.org/project/cookie_content_blocker) or [gdpr_video](https://www.drupal.org/project/gdpr_video)
5. Do we want to keep google_tag as a dependency? If not, remove from .info & composer.json files, and delete optional config.
6. Do we keep using eucc, or is it time to look at [cookies](https://www.drupal.org/project/cookies/)

## Credits
Parts are based or inspired on the following modules:
- [eu_cookie_compliance_rocketship](https://www.drupal.org/project/eu_cookie_compliance_rocketship)
- [eu_cookie_compliance_gtm](https://www.drupal.org/project/eu_cookie_compliance_gtm)
- [ac-eu-cookie-compliance-tweaks](https://gitlab.agile.coop/agile-public/ac-eu-cookie-compliance-tweaks)
